def is_prime?(num)
  half=num/2 #eg: 5/2=2 , 10/2=5
  result = true
  2.upto(half) do |k|
    if(num%k == 0)
      result = false
      break
    end
  end
  return result
end

i=2 #1 is niether prime nor composite so starting from 2
primes = []
prime_count=0

while prime_count<=100
  if is_prime?(i)
    primes << i
    prime_count+=1
  end
  i+=1
end

1.upto(100) do |j|
  puts "#{j}. #{primes[j]}"
end
